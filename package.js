Package.describe({
  name: "hsed:body-events",
  summary: "Get Template.body.events() working",
  version: "1.0.0",
  git: "https://bitbucket.org/hsedevelopment/meteor-body-events"
});

Package.onUse(function(api, where) {
  api.versionsFrom("2.5");
  api.use(["templating", "jquery"], "client");
  api.addFiles(["lib.js"], "client");
});
